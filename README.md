# A. INSTALL PREREQUISITES:

- Install Virtualbox and Vagrant, and Ansible
- At working directory, call `vagrant up` and wait for the command to spin up 4 virtual machines: 2 for staging, 2 for production
```
	hosts = {
	  "staging_dbserver1" => "192.168.10.2",
	  "staging_webserver1" => "192.168.10.3",
	  "production_dbserver1" => "192.168.11.2",
	  "production_webserver1" => "192.168.11.3"
	}
```

- To provision staging environment `$ ansible-playbook -i inventory_staging site.yml` and then visit: `192.168.10.3` to complete the setup with the following credentials:

```
DB name: wordpress
DB username: wordpress
DB password: toor
DB host: 192.168.10.2
```

- To provision production environment, modify `inventory_production` with associated credentials and call `$ ansible-playbook -i inventory_production site.yml` and then visit to the web server to complete the setup with the following credentials:

```
DB name: wordpress
DB username: wordpress
DB password: passw0rd
```


# B. APPROACH & DESIGN

- Seperate database servers and web servers
- Group servers by geographic location:

```
	TORONTO (geo):
		- toronto-webservers:
			+ toronto_staging_webserver1
			+ toronto_staging_webserver2
		- toronto-dbservers:
			+ toronto_staging_dbserver1
			+ toronto_staging_dbserver2
```
- Group servers by function:
```
	WEBSERVER:
		- toronto-webservers
		- newyork-webservers
	DATABASE:
		- toronto-dbservers
		- newyork-dbservers
```

## Inventory sample
```
[toronto-webservers]
toronto_staging_webserver1	ansible_host=192.168.10.3	ansible_user=vagrant	ansible_connection=ssh ansible_ssh_private_key_file=.vagrant/machines/staging_webserver1/virtualbox/private_key


[toronto-dbservers]
toronto_staging_dbserver1 	ansible_host=192.168.10.2	ansible_user=vagrant	ansible_connection=ssh ansible_ssh_private_key_file=.vagrant/machines/staging_dbserver1/virtualbox/private_key


# webservers in all geos
[webservers:children]
toronto-webservers

# dbservers in all geos
[dbservers:children]
toronto-dbservers

# everything in the toronto geo
[toronto:children]
toronto-webservers
toronto-dbservers
```




# C. WHAT COULD BE BETTER:

1. Skip common task on subsequent call for faster execution time (play only once)
2. Customize installation to fit the environment WeCharity (MySQL version, WordPress version, PHP and PHPfpm version)
3. Backup database, bootstrap database, bootstrap WordPress installation. To prevent downtime, pay extra attention to Ansible execution stategy/
4. Use Ansible Vault to encrypt credentials


Infrastructure as code - automate with Terraform:

1. Install cache server, CDN for statics
2. Firewall setup - database is open to the world. We will rely on firewall provided by cloud service. If self hosted, database should be on another volume and the volume should be encrypted
